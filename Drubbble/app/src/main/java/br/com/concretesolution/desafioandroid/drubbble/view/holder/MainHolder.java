package br.com.concretesolution.desafioandroid.drubbble.view.holder;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.TypedValue;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.paging.listview.PagingListView;

import br.com.concretesolution.desafioandroid.drubbble.R;
import br.com.concretesolution.desafioandroid.drubbble.view.interfaces.IListenerActivity;
import br.com.concretesolution.desafioandroid.drubbble.view.widget.DrubbbleSwipeRefreshLayout;

/**
 * 
 * @author Giuliano Paladino
 *
 */

public class MainHolder extends AbstractHolder {

	private DrubbbleSwipeRefreshLayout swipeRefreshLayout;
	private PagingListView listDrubbble;
	private TextView txtNotListDrubbles;
	private ProgressBar progressBarForListDrubbbles;

	public MainHolder(IListenerActivity iListener) {
		super(iListener);
	}

	@Override
	protected void initializeFields() {
		swipeRefreshLayout = (DrubbbleSwipeRefreshLayout) getField(R.id.container);
		listDrubbble = (PagingListView) getField(R.id.listDrubbles);
		txtNotListDrubbles = (TextView) getField(R.id.tv_no_drubbles);
		progressBarForListDrubbbles = (ProgressBar) getField(R.id.progressBarForListDrubbbles);
	}

	public DrubbbleSwipeRefreshLayout getSwipeRefreshLayout() {
		return swipeRefreshLayout;
	}

	public PagingListView getListDrubbble() {
		return listDrubbble;
	}

	public TextView getTxtNotListDrubbles() {
		return txtNotListDrubbles;
	}

	public ProgressBar getProgressBarForListDrubbbles() {
		return progressBarForListDrubbbles;
	}
}
