package br.com.concretesolution.desafioandroid.drubbble.model.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * 
 * @author Giuliano Paladino
 * 
 */
public class UtilNetwork {

	public static final int TYPE_WIFI = 1;
	public static final int TYPE_MOBILE = 2;
	public static final int TYPE_NOT_CONNECTED = 0;
	public static final String WIFI_ENABLE = "Wifi enabled";
	public static final String MOBILE_ENABLE = "Mobile data enabled";
	public static final String NOT_CONNECTED = "Not connected to Internet";

	/**
	 * 
	 * @param context
	 * @return code connectivity
	 */
	public static int getConnectivityStatus(final Context context) {
		final ConnectivityManager cm = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		final NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
		if (null != activeNetwork) {
			if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
				return TYPE_WIFI;
			} else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
				return TYPE_MOBILE;
			}
		}
		return TYPE_NOT_CONNECTED;
	}

	/**
	 * return connectivity status string
	 * 
	 * @param context
	 * @return
	 */
	public static String getConnectivityStatusString(final Context context) {
		String status = null;
		int conn = UtilNetwork.getConnectivityStatus(context);
		switch (conn) {
		case TYPE_WIFI:
			status = WIFI_ENABLE;
			break;
		case TYPE_MOBILE:
			status = MOBILE_ENABLE;
			break;
		case TYPE_NOT_CONNECTED:
			status = NOT_CONNECTED;
			break;
		default:
			status = NOT_CONNECTED;
			break;
		}
		return status;
	}
}
