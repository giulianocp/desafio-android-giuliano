package br.com.concretesolution.desafioandroid.drubbble.model.utils;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Point;
import android.util.DisplayMetrics;

public final class DisplayUtils {

	private DisplayUtils() {
		// Avoid instance
	}

	public static float getDisplayDensity(final Context context){
		final DisplayMetrics metrics = context.getResources()
				.getDisplayMetrics();
		return metrics.density;
	}

	/**
	 * Get the current display size.
	 * 
	 * @param context
	 * @return
	 */
	public static Point getDisplaySize(final Context context) {
		final DisplayMetrics metrics = context.getResources()
				.getDisplayMetrics();
		return new Point(metrics.widthPixels, metrics.heightPixels);
	}

	/**
	 * Get the display size in Portrait.
	 * 
	 * @param context
	 * @return
	 */
	public static Point getPortraitDisplaySize(final Context context) {
		final DisplayMetrics metrics = context.getResources()
				.getDisplayMetrics();
		if (metrics.widthPixels > metrics.heightPixels) {
			// Landscape
			return new Point(metrics.heightPixels, metrics.widthPixels);
		} else {
			// Portrait
			return new Point(metrics.widthPixels, metrics.heightPixels);
		}
	}

	public static void lockPortrait(final Activity activity) {
		activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	}

	public static void lockLandscape(final Activity activity) {
		activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
	}

	public static void unlockOrientation(final Activity activity) {
		activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);
	}
}
