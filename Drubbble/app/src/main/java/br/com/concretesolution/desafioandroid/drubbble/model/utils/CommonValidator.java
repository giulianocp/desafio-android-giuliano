package br.com.concretesolution.desafioandroid.drubbble.model.utils;


import android.annotation.SuppressLint;
import android.text.TextUtils;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class CommonValidator {

	private static final int MIN_SIZE_PASSWORD = 8;
	private static final String EMAIL_PATTERN = ".+@.+\\..+";
	private static final int CPF_MAX_INDEX = 11;
	private static final int PHONE_MAX_INDEX = 10;

	public static boolean isValidCPF(final String text) {
		String cpf = text;
		cpf = cpf.replace(".", "");
		cpf = cpf.replace("-", "");
		cpf = cpf.trim();

		if (cpf == null || cpf.length() != CPF_MAX_INDEX || isCPFPadrao(cpf))
			return false;

		try {
			Long.parseLong(cpf);
		} catch (NumberFormatException e) { // CPF n�o possui somente
											// n�meros
			return false;
		}

		return calcDigVerif(cpf.substring(0, 9)).equals(
				cpf.substring(9, CPF_MAX_INDEX));
	}

	/**
	 * '
	 * 
	 * @param cpf
	 *            String valor a ser testado
	 * @return boolean indicando se o usuario entrou com um CPF padrao
	 */
	private static boolean isCPFPadrao(String cpf) {
		if (cpf.equals("11111111111") || cpf.equals("22222222222")
				|| cpf.equals("33333333333") || cpf.equals("44444444444")
				|| cpf.equals("55555555555") || cpf.equals("66666666666")
				|| cpf.equals("77777777777") || cpf.equals("88888888888")
				|| cpf.equals("99999999999")) {

			return true;
		}
		return false;
	}


	private static String calcDigVerif(final String num) {
		Integer primDig, segDig;
		int soma = 0, peso = 10;
		for (int i = 0; i < num.length(); i++)
			soma += Integer.parseInt(num.substring(i, i + 1)) * peso--;

		if (soma % CPF_MAX_INDEX == 0 || soma % CPF_MAX_INDEX == 1) {
			primDig = Integer.valueOf(0);
		} else {
			primDig = Integer.valueOf(CPF_MAX_INDEX - (soma % CPF_MAX_INDEX));
		}

		soma = 0;
		peso = CPF_MAX_INDEX;
		for (int i = 0; i < num.length(); i++) {
			soma += Integer.parseInt(num.substring(i, i + 1)) * peso--;
		}
		soma += primDig.intValue() * 2;
		if (soma % CPF_MAX_INDEX == 0 || soma % CPF_MAX_INDEX == 1) {
			segDig = Integer.valueOf(0);
		} else {
			segDig = Integer.valueOf(CPF_MAX_INDEX - (soma % CPF_MAX_INDEX));
		}

		return primDig.toString() + segDig.toString();
	}

	public final static boolean isValidEmail(String email) {
		final Pattern pattern = Pattern.compile(EMAIL_PATTERN);
		final Matcher matcher = pattern.matcher(email);
		if (matcher.matches()) {
			return true;
		}
		return false;
	}

	public static boolean isValidPhone(String phone) {
		phone = phone.replaceAll("[^0-9]*", "");
		if (phone == null || phone.length() < PHONE_MAX_INDEX)
			return false;
		try {
			Long.parseLong(phone);
		} catch (NumberFormatException e) { 
			return false;
		}
		return true;

	}

	@SuppressLint("SimpleDateFormat")
	public static boolean isDateValid(final String date, final String pattern) {
		boolean isValid = false;
		try {
			final SimpleDateFormat format = new SimpleDateFormat(pattern);
			final Calendar specificDate = Calendar.getInstance();
			specificDate.setTime(format.parse(date));
			isValid = true;
		} catch (Exception e) {
			isValid = false;
		}
		return isValid;
	}

}
