package br.com.concretesolution.desafioandroid.drubbble.model.ws;

import android.content.Context;

import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import br.com.concretesolution.desafioandroid.drubbble.model.bo.DrubbbleJson;
import br.com.concretesolution.desafioandroid.drubbble.model.utils.SharedPreferencesUtils;
import br.com.concretesolution.desafioandroid.drubbble.presenter.PresenterListDrubbble;
import br.com.concretesolution.desafioandroid.drubbble.model.utils.GsonRequest;

public class DrubbleWS extends AbstractWS<DrubbbleJson>{

    private RequestQueue requestQueue;
    private PresenterListDrubbble delegate;
	private int pager;
	private Context context;
    
    public DrubbleWS(PresenterListDrubbble delegate, Context context, int pager){
		this.delegate = delegate;
		this.pager = pager;
		this.context = context;
		this.requestQueue = Volley.newRequestQueue(context);
    }
    
    @Override
    public void startRequest() {
	GsonRequest<DrubbbleJson> jsObjRequest = new GsonRequest<DrubbbleJson>(
		Method.GET,
		"http://api.dribbble.com/shots/popular?page="+pager,
			DrubbbleJson.class, null,
		this.createSuccessListener(),
		this.createErrorListener());
	    this.requestQueue.add(jsObjRequest);
    }

    @Override
    public Listener<DrubbbleJson> createSuccessListener() {
	return new Listener<DrubbbleJson>() {
	    @Override
	    public void onResponse(DrubbbleJson response) {
			SharedPreferencesUtils.writePreferences(context, "pager", response.getPages());
			delegate.notifyFinish(response);
	    }
	};
    }

    @Override
    public ErrorListener createErrorListener() {
	return new ErrorListener() {
	    @Override
	    public void onErrorResponse(VolleyError error) {
		    delegate.notifyConnectionFailure(error);
	    }
	};
    }

    @Override
    public void onDestroy() {
	this.requestQueue.cancelAll(this);
    }

}
