package br.com.concretesolution.desafioandroid.drubbble.model.bo;

import java.util.List;


/**
 * 
 * @author Giuliano Paladino
 * 
 */


public class DrubbbleJson {

	private Integer pages;
	private List<DrubbbleBO> shots;

	public DrubbbleJson(){
		super();
	}

	public DrubbbleJson(Integer pages, final List<DrubbbleBO> shots) {
		super();
		this.pages = pages;
		this.shots = shots;
	}

	public List<DrubbbleBO> getShots() {
		return shots;
	}

	public void setShots(List<DrubbbleBO> shots) {
		this.shots = shots;
	}

	public Integer getPages() {
		return pages;
	}

	public void setPages(Integer pages) {
		this.pages = pages;
	}
}
