package br.com.concretesolution.desafioandroid.drubbble.model.utils;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;

public class FieldNameExclusionStrategy implements ExclusionStrategy {

	private final String[] excludedFields;
	
	public FieldNameExclusionStrategy(final String... excludedFields) {
		this.excludedFields = excludedFields;
	}
	
	@Override
	public boolean shouldSkipClass(Class<?> arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean shouldSkipField(FieldAttributes field) {
		for (String excludedName : excludedFields) {
			if(field.getName().equals(excludedName)) {
				return true;
			}	
		}
		return false;
	}

}
