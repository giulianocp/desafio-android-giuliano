package br.com.concretesolution.desafioandroid.drubbble.view.interfaces;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;

/**
 * 
 * @author Giuliano Paladino
 * 
 */
public interface IListenerActivity {

    /**
     * @see Activity#getBaseContext()
     * @return {@link Context}
     */
    public Context getBaseContext();

    /**
     * @see Activity#getApplicationContext()
     * @return {@link Context}
     */
    public Context getApplicationContext();

    /**
     * @see Activity#getAssets()
     * @return {@link AssetManager}
     */
    public AssetManager getAssets();

    /**
     * @see Activity#getString(int)
     * @param resId
     * @return {@link String}
     */
    public String getString(final int resId);

    /**
     * @see Activity#getResources()
     * @return {@link Resources}
     */
    public Resources getResources();

    /**
     * @see Activity#findViewById(int)
     * @param id
     * @return {@link View}
     */
    public View findViewById(final int id);

    /**
     * @see Activity#getSharedPreferences(String, int)
     * @param name
     * @param mode
     * @return {@link SharedPreferences}
     */
    public SharedPreferences getSharedPreferences(final String name, final int mode);
    
    public LayoutInflater getLayoutInflater();
}
