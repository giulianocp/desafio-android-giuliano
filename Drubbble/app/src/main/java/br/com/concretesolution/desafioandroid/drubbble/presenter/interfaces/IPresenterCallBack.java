package br.com.concretesolution.desafioandroid.drubbble.presenter.interfaces;


import com.android.volley.VolleyError;

import java.util.List;

import br.com.concretesolution.desafioandroid.drubbble.model.bo.DrubbbleJson;

/**
 * 
 * @author Giuliano Paladino
 *
 */

public interface IPresenterCallBack {

	/**
     * send notification on connection failure
     */
    void notifyConnectionFailure(VolleyError error);

    /**
     * send notification has start
     */
    void notifyStart();

    /**
     * send notification has finish
     */
    void notifyFinish(DrubbbleJson response);
    
}
