package br.com.concretesolution.desafioandroid.drubbble.view.adapter;

import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import br.com.concretesolution.desafioandroid.drubbble.R;
import br.com.concretesolution.desafioandroid.drubbble.model.bo.DrubbbleBO;

public class MainAdapter extends AbstractAdapter<DrubbbleBO>{

	public MainAdapter(final Context context, final List<DrubbbleBO> mListDrubbble) {
		super(mListDrubbble, context);
	}

	public void setListDrubbble(final List<DrubbbleBO> mListDrubbble) {
		getItems().clear();
		getItems().addAll(mListDrubbble);
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(final int position, View convertView, final ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			convertView = LayoutInflater.from(getContext()).inflate(R.layout.drubbbles_list_item, null);
			holder = new ViewHolder(convertView);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		final DrubbbleBO drubbbleBO = getItem(position);
		holder.putValues(drubbbleBO, position);

		return convertView;
	}

	class ViewHolder {

		private ImageView imgDrubbble;
		private TextView tvName;

		public ViewHolder(final View view) {
			imgDrubbble = (ImageView) view.findViewById(R.id.picture);
			tvName = (TextView) view.findViewById(R.id.tv_name);
		}

		public void putValues(final DrubbbleBO drubbbleBO, final int position) {

			if(drubbbleBO == null || position < 0) {
				return;
			}

			tvName.setText(drubbbleBO.getTitle());
			Picasso.with(getContext()).load(drubbbleBO.getImage_url()).into(imgDrubbble);

		}

	}

}
