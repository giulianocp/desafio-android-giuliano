package br.com.concretesolution.desafioandroid.drubbble.view.activity;

import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.squareup.picasso.Picasso;

import br.com.concretesolution.desafioandroid.drubbble.R;
import br.com.concretesolution.desafioandroid.drubbble.model.bo.DrubbbleBO;
import br.com.concretesolution.desafioandroid.drubbble.view.holder.DrubbbleDetailsHolder;
import br.com.concretesolution.desafioandroid.drubbble.view.interfaces.IListenerActivity;

public class DrubbbleDetailsActivity extends Activity implements IListenerActivity {

    private DrubbbleDetailsHolder mHolder;
    private DrubbbleBO drubbbleBO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drubbbles_details);
        mHolder = new DrubbbleDetailsHolder(this);

        ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setHomeButtonEnabled(true);

        initializeActions();
    }

    private void initializeActions() {
        if (getIntent() != null) {
            drubbbleBO = (DrubbbleBO) getIntent().getSerializableExtra("DrubbblesBO");
            if(drubbbleBO != null) {
                Picasso.with(this).load(drubbbleBO.getImage_url()).into(mHolder.getImgPicture());
                mHolder.getTxtTitle().setText(drubbbleBO.getTitle());
                mHolder.getTxtName().setText(drubbbleBO.getPlayer().getName());
                mHolder.getTxtDescription().setText(drubbbleBO.getDescription());
                Picasso.with(this)
                        .load(drubbbleBO.getPlayer().getAvatar_url())
                        .resize(50, 50)
                        .centerCrop()
                        .into(mHolder.getImgPlayer());
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == android.R.id.home){
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
