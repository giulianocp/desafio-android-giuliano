package br.com.concretesolution.desafioandroid.drubbble.presenter;

import android.content.Context;

import com.android.volley.VolleyError;
import br.com.concretesolution.desafioandroid.drubbble.model.bo.DrubbbleJson;
import br.com.concretesolution.desafioandroid.drubbble.model.ws.DrubbleWS;
import br.com.concretesolution.desafioandroid.drubbble.presenter.interfaces.IPresenterCallBack;

/**
 * 
 * @author Giuliano Paladino
 *
 */

public class PresenterListDrubbble {

	private final IPresenterCallBack mIPresenterCall;
	private final Context context;

	public PresenterListDrubbble(final IPresenterCallBack mIPresenterCall, final Context context) {
		this.mIPresenterCall = mIPresenterCall;
		this.context = context;
	}

	public void notifyConnectionFailure(VolleyError error) {
		mIPresenterCall.notifyConnectionFailure(error);
	}

	public void notifyStart(int pager) {
		mIPresenterCall.notifyStart();
		new DrubbleWS(this, context, pager).startRequest();

	}

	public void notifyFinish(DrubbbleJson response) {
		mIPresenterCall.notifyFinish(response);
	}
}
