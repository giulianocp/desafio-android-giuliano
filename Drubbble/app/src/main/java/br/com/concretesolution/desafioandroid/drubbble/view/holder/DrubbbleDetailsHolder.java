package br.com.concretesolution.desafioandroid.drubbble.view.holder;

import android.media.Image;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.paging.listview.PagingListView;

import br.com.concretesolution.desafioandroid.drubbble.R;
import br.com.concretesolution.desafioandroid.drubbble.view.interfaces.IListenerActivity;
import br.com.concretesolution.desafioandroid.drubbble.view.widget.DrubbbleSwipeRefreshLayout;

/**
 * 
 * @author Giuliano Paladino
 *
 */

public class DrubbbleDetailsHolder extends AbstractHolder {

	private ImageView imgPicture;
	private TextView txtTitle;
	private ImageView imgPlayer;
	private TextView txtName;
	private TextView txtDescription;


	public DrubbbleDetailsHolder(IListenerActivity iListener) {
		super(iListener);
	}

	@Override
	protected void initializeFields() {
		imgPicture = (ImageView) getField(R.id.picture);
		txtTitle = (TextView) getField(R.id.tv_title);
		imgPlayer = (ImageView) getField(R.id.imagePlayer);
		txtName = (TextView) getField(R.id.tv_name);
		txtDescription = (TextView) getField(R.id.tv_description);

	}

	public ImageView getImgPicture() {
		return imgPicture;
	}

	public TextView getTxtTitle() {
		return txtTitle;
	}

	public ImageView getImgPlayer() {
		return imgPlayer;
	}

	public TextView getTxtName() {
		return txtName;
	}

	public TextView getTxtDescription() {
		return txtDescription;
	}
}