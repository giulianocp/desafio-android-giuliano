package br.com.concretesolution.desafioandroid.drubbble.view.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;

import com.android.volley.VolleyError;
import com.paging.listview.PagingListView;

import java.util.List;

import br.com.concretesolution.desafioandroid.drubbble.R;
import br.com.concretesolution.desafioandroid.drubbble.model.bo.DrubbbleBO;
import br.com.concretesolution.desafioandroid.drubbble.model.bo.DrubbbleJson;
import br.com.concretesolution.desafioandroid.drubbble.model.utils.SharedPreferencesUtils;
import br.com.concretesolution.desafioandroid.drubbble.presenter.PresenterListDrubbble;
import br.com.concretesolution.desafioandroid.drubbble.presenter.interfaces.IPresenterCallBack;
import br.com.concretesolution.desafioandroid.drubbble.view.adapter.MainAdapter;
import br.com.concretesolution.desafioandroid.drubbble.view.holder.MainHolder;
import br.com.concretesolution.desafioandroid.drubbble.view.interfaces.IListenerActivity;


public class MainActivity extends Activity implements IPresenterCallBack, IListenerActivity {

    private PresenterListDrubbble mPresenter;
    private MainHolder mHolder;
    private MainAdapter mAdapter;
    private List<DrubbbleBO> listaDrubbbles;
    private int pager = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mPresenter = new PresenterListDrubbble(this, this);
        mHolder = new MainHolder(this);
        inilizateAction();
    }

    private void inilizateAction() {
        mHolder.getSwipeRefreshLayout().setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        pager--;
                        if(pager > 1) {
                            mPresenter.notifyStart(pager);
                        }else{
                            mHolder.getSwipeRefreshLayout().setRefreshing(false);
                        }
                    }
                }
        );

        mHolder.getListDrubbble().setHasMoreItems(true);
        mHolder.getListDrubbble().setPagingableListener(new PagingListView.Pagingable() {
            @Override
            public void onLoadMoreItems() {
                pager++;
                if (pager < SharedPreferencesUtils.getPreferencesInteger(MainActivity.this, "pager")) {
                    mPresenter.notifyStart(pager);
                } else {
                    mHolder.getListDrubbble().onFinishLoading(false, null);
                }
            }
        });

        mHolder.getListDrubbble().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                DrubbbleBO drubbbleBO = listaDrubbbles.get(position);
                Intent it = new Intent(MainActivity.this, DrubbbleDetailsActivity.class);
                it.putExtra("DrubbblesBO", drubbbleBO);
                startActivity(it);
            }
        });

        mPresenter.notifyStart(pager);
    }

    public void setAdapterList() {
        if(listaDrubbbles != null && listaDrubbbles.size() > 0){
            if (mAdapter == null) {
                mAdapter = new MainAdapter(this, listaDrubbbles);
            } else {
                mAdapter.setListDrubbble(listaDrubbbles);
            }
            mAdapter.notifyDataSetChanged();
            mHolder.getListDrubbble().setAdapter(mAdapter);
        }
    }

    @Override
    public void notifyConnectionFailure(VolleyError error) {
        mHolder.getTxtNotListDrubbles().setVisibility(View.VISIBLE);
        mHolder.getProgressBarForListDrubbbles().setVisibility(View.GONE);
    }

    @Override
    public void notifyStart() {
        mHolder.getProgressBarForListDrubbbles().setVisibility(View.VISIBLE);
    }

    @Override
    public void notifyFinish(DrubbbleJson response) {
        this.listaDrubbbles = response.getShots();
        setAdapterList();
        mHolder.getSwipeRefreshLayout().setRefreshing(false);
        mHolder.getListDrubbble().onFinishLoading(true, listaDrubbbles);
        mHolder.getProgressBarForListDrubbbles().setVisibility(View.GONE);
    }
}