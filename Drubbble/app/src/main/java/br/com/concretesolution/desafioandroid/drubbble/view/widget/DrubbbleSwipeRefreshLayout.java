package br.com.concretesolution.desafioandroid.drubbble.view.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.AttributeSet;
import android.widget.AdapterView;
import br.com.concretesolution.desafioandroid.drubbble.R;

public class DrubbbleSwipeRefreshLayout extends SwipeRefreshLayout {
	
	private static final int DEFAULT_NO_ADAPTER_VIEW = -1;
	private static final int UP_DIRECTION = -1;
	private int mAdapterViewId = DEFAULT_NO_ADAPTER_VIEW;
	
	@SuppressWarnings("rawtypes")
	private AdapterView adapterView;
	
	public DrubbbleSwipeRefreshLayout(final Context context) {
		super(context);
	}

	public DrubbbleSwipeRefreshLayout(final Context context, final AttributeSet attrs) {
		super(context, attrs);
		customize(attrs);
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();
		if(mAdapterViewId != DEFAULT_NO_ADAPTER_VIEW) {
			adapterView = (AdapterView) findViewById(mAdapterViewId);
		}
	}
	
	@Override
	public boolean canChildScrollUp() {
		if(adapterView == null) {
			return super.canChildScrollUp();
		} else {
			return adapterView.canScrollVertically(UP_DIRECTION);
		}
	}
	
	private void customize(final AttributeSet attrs) {
		final TypedArray styledAttributes = getContext().obtainStyledAttributes(attrs, R.styleable.SwipeRefreshLayout);
		mAdapterViewId = styledAttributes.getResourceId(R.styleable.SwipeRefreshLayout_adapter_view, DEFAULT_NO_ADAPTER_VIEW);
		styledAttributes.recycle();
	}
	
	@SuppressWarnings("rawtypes")
	public void setCurrentAdapterView(final AdapterView adapterView) {
		this.adapterView = adapterView;
	}

}
