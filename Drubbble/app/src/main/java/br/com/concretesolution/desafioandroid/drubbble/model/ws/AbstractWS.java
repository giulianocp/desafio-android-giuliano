package br.com.concretesolution.desafioandroid.drubbble.model.ws;

import com.android.volley.Response;

/**
 * 
 * @author Giuliano Paladino
 * 
 */


public abstract class AbstractWS<E> {

    public abstract void startRequest();

    /**
     * create success in parser
     * 
     */
    public abstract Response.Listener<E> createSuccessListener();

    /**
     * create error in parser
     * 
     */
    public abstract Response.ErrorListener createErrorListener();
    
    
    public abstract void onDestroy();

}
